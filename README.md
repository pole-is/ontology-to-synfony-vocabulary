# Ontology-to-synfony-vocabulary

## Ce projet est un script permettant de sémantiser, au sens ontologique, une application Synfony

### Description

Ce projet, utilisé en conjonction d'une application Synfony, permet de créer un fichier de configuration pour API platform en mettant en relation les resources servies par l'API Synfony avec des resources décrites dans un ou plusieurs fichier(s) owl/rdf.

Le programme est codé en typescript, et peut donc être `build` avec `npm run build`, et même être compilé dans une version exécutable avec l'utilitaire nexe et la commande `npm run make` ou `npm run make-linux`. Dans les deux cas, le programe est un CLI avec une aide. Un dockerfile est également présent pour contruire une image.

Les paramètres attendus sont:
 - **ontologyPath**: Chemin vers un fichier ou dossier contenant des ressources sémantiques
 - **synfonyEntitiesPath**: Chemin vers un fichier contenant la liste des entités Synfony pleinement qualifié
 - **outputPath**: Chemin vers le fichier de configuration .yaml pour APIplatform


```mermaid
sequenceDiagram
    participant Fichier_OWL_RDF as Fichier OWL/RDF ou Dossier
    participant Entites_PHP as Fichier Entités PHP Symfony
    participant ETL_Program as Ontology-to-synfony-vocabulary
    participant Fichier_Config as Fichier Configuration .yaml

    Fichier_OWL_RDF->>ETL_Program: Envoie des données OWL/RDF
    Entites_PHP->>ETL_Program: Envoie des noms d'entités
    ETL_Program->>Fichier_Config: Génère le fichier .yaml
    Note right of ETL_Program: Traitement des données
    Note right of Fichier_Config: Utilisé pour API Platform
```
### Instructions

L'utilisation final de ce projet est d'être utilisé en tant que job d'un processus de CI/CD du projet Synfony. Les prérequis sont:
 - Un dépot, ou autre source, contenant des ressources sémantiques, dans un format supporté par la libraire `rdf-parse`
 - Un projet Synfony, avec une commande pour généré la liste des entités avec l'autorité complète
 - Un job gitlab / action github dans le projet Synfony qui récupère le contenu de ce dépôt et effectue le programme
 
#### Pour les dévelopeurs

Il est possible d'utiliser une librairie comme ts-node (installé globalement) pour rapidement déboger. La commande pour lancer le programme est alors:

`ts-node ./index.ts --help`

### Contenus

 - Le code principal se trouve dans le fichier index.ts et le dossier src/.
 - Une série de tests se trouve dans le dosser /tests.
 - Un exemple de code PHP pour la commande de liste des entités pleinement qualifié, ainsi qu'un exemple de job .gitlab minimal, est dans le dossier ./example.
