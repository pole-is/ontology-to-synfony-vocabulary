const nexe = require('nexe');
const PATH_TO_APP = __dirname + '/dist/index.js'
async function compile() {
  try {
    // Compilation pour Linux
    await nexe.compile({
      input: PATH_TO_APP, // Chemin vers le fichier principal de votre application
      output: 'ontology-to-synfony-vocabulary.bin', // Nom du fichier de sortie pour Linux
      target: 'linux-x64', // Spécifiez la cible pour Linux (architecture, version de Node.js)
      build: true
    });
    console.log('Compilation pour Linux terminée');

    // // Compilation pour Windows
    // await nexe.compile({
    //   input: PATH_TO_APP, // Chemin vers le fichier principal de votre application
    //   output: 'ontology-to-synfony-vocabulary.exe', // Nom du fichier de sortie pour Windows
    //   target: 'windows-x64' // Spécifiez la cible pour Windows (architecture, version de Node.js)
    // });
    // console.log('Compilation pour Windows terminée');

  } catch (error) {
    console.error('Erreur lors de la compilation:', error);
  }
}

compile();
