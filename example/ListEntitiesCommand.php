<?php declare(strict_types=1);
/*
 * Copyright (C) 2017-2021 IRSTEA
 * All rights reserved.
 */

// src/Command/ListEntitiesCommand.php

namespace App\Command;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListEntitiesCommand extends Command
{
    protected static $defaultName = 'app:list-entities';
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Lists all entities in the system with their full authority.')
            ->setHelp('This command lists all entities in the system with their full authority.');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->doctrine->getManager();
        $metadata = $entityManager->getMetadataFactory()->getAllMetadata();

        foreach ($metadata as $classMetadata) {
            $output->writeln($classMetadata->getName());
        }

        return 0;
    }
}
